import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateLegueDto {
  @ApiProperty({ example: 'LaLiga', description: 'Ligue name' })
  @IsString()
  name: string;
}
