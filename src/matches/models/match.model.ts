import { ApiProperty } from '@nestjs/swagger';
import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';
import { Legue } from '../../legues/models/legue.model';
import { Team } from '../../teams/model/team.model';
import { WeekDay } from '../../week_days/model/week_day.model';

@Table({ tableName: 'matches' })
export class Match extends Model<Match> {
  @ApiProperty({ example: 1, description: 'Primary key' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  })
  id: number;

  @ApiProperty({ example: 1, description: 'Foreign key, legue' })
  @ForeignKey(() => Legue)
  @Column({
    type: DataType.INTEGER,
  })
  legue_id: number;

  @BelongsTo(() => Legue)
  legue: Legue;

  @ApiProperty({ example: 1, description: 'Foreign key, team' })
  @ForeignKey(() => Team)
  @Column({
    type: DataType.INTEGER,
  })
  host_id: number;

  @BelongsTo(() => Team)
  host: Team;

  @ApiProperty({ example: 1, description: 'Foreign key, team' })
  @ForeignKey(() => Team)
  @Column({
    type: DataType.INTEGER,
  })
  gust_id: number;

  @BelongsTo(() => Team)
  gust: Team;

  @ApiProperty({ example: 1, description: 'Score' })
  @Column({
    type: DataType.STRING,
  })
  score: string;

  @ApiProperty({ example: '2023-06-1', description: 'Date' })
  @Column({
    type: DataType.DATE,
    defaultValue: Date.now(),
  })
  date: Date;

  @ApiProperty({ example: '11:00', description: 'Time' })
  @Column({
    type: DataType.DATE,
  })
  time: Date;

  @ApiProperty({ example: 1, description: 'Foreign key, week day' })
  @ForeignKey(() => WeekDay)
  @Column({
    type: DataType.INTEGER,
  })
  week_days_id: number;

  @BelongsTo(() => WeekDay)
  week_day: Team;
}
