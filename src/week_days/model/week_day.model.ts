import { ApiProperty } from '@nestjs/swagger';
import { Column, DataType, Model, Table } from 'sequelize-typescript';

@Table({ tableName: 'week_days' })
export class WeekDay extends Model<WeekDay> {
  @ApiProperty({ example: 1, description: 'Primary key' })
  @Column({
    type: DataType.SMALLINT,
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @ApiProperty({ example: 1, description: 'Week day name' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;
}
