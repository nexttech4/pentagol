import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpdateMatchDto {
  @ApiProperty({ example: 1, description: 'Foreign key, legue' })
  @IsOptional()
  @IsNumber()
  readonly legue_id?: number;

  @ApiProperty({ example: 1, description: 'Foreign key, team' })
  @IsOptional()
  @IsNumber()
  readonly host_id?: number;

  @ApiProperty({ example: 1, description: 'Foreign key, team' })
  @IsOptional()
  @IsNumber()
  readonly gost_id?: number;

  @ApiProperty({ example: '3:0', description: 'Score' })
  @IsOptional()
  @IsString()
  readonly score?: string;

  @ApiProperty({ example: '2023-06-01', description: 'Date' })
  @IsOptional()
  @IsDateString()
  readonly date?: Date;

  @ApiProperty({ example: '11:00', description: 'Time' })
  @IsOptional()
  readonly time?: Date;

  @ApiProperty({ example: 1, description: 'Foreign key, week day' })
  @IsOptional()
  readonly week_days_id?: number;
}
