import { PartialType } from '@nestjs/mapped-types';
import { CreateAdminDto } from './create-admin.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateAdminDto {
  @ApiProperty({ example: '1abcdef', description: 'Admin login' })
  @IsNotEmpty()
  @IsString()
  login: string;
}
