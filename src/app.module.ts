import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';
import { TeamsModule } from './teams/teams.module';
import { AdminModule } from './admin/admin.module';
import { LeguesModule } from './legues/legues.module';
import { WeekDaysModule } from './week_days/week_days.module';
import { MatchesModule } from './matches/matches.module';
import { NewsModule } from './news/news.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: `.env`,
      isGlobal: true,
    }),
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: process.env.POSTGRES_HOST, //'localhost',
      port: Number(process.env.POSTGRES_PORT),
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
      autoLoadModels: true,
      synchronize: true,
      logging: false,
    }),
    TeamsModule,
    AdminModule,
    LeguesModule,
    WeekDaysModule,
    MatchesModule,
    NewsModule,
  ],
})
export class AppModule {}
