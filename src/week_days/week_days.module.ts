import { Module } from '@nestjs/common';
import { WeekDaysService } from './week_days.service';
import { WeekDaysController } from './week_days.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { WeekDay } from './model/week_day.model';

@Module({
  imports: [SequelizeModule.forFeature([WeekDay])],
  controllers: [WeekDaysController],
  providers: [WeekDaysService],
})
export class WeekDaysModule {}
