import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateLegueDto } from './dto/create-legue.dto';
import { UpdateLegueDto } from './dto/update-legue.dto';
import { InjectModel } from '@nestjs/sequelize';
import { Legue } from './models/legue.model';

@Injectable()
export class LeguesService {
  constructor(@InjectModel(Legue) private readonly legueRepo: typeof Legue) {}
  async create(createLegueDto: CreateLegueDto) {
    const legue = await this.legueRepo.findOne({
      where: { name: createLegueDto.name },
    });
    if (legue) {
      throw new BadRequestException('Legue already exists');
    }
    const newLegue = await this.legueRepo.create(createLegueDto);
    const response = {
      message: 'Legue created successfully',
      legue: newLegue,
    };
    return response;
  }

  findAll() {
    return this.legueRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const legue = await this.legueRepo.findOne({
      where: { id: id },
      include: { all: true },
    });
    if (!legue) {
      throw new NotFoundException('Legue not found');
    }
    return legue;
  }

  async update(id: number, updateLegueDto: UpdateLegueDto) {
    const legue = await this.legueRepo.findOne({ where: { id } });
    if (!legue) {
      throw new NotFoundException('Legue not found');
    }
    const updateLegue = await this.legueRepo.update(
      { ...updateLegueDto },
      { where: { id }, returning: true },
    );
    return updateLegue[1][0];
  }

  async remove(id: number) {
    const legue = await this.legueRepo.findOne({ where: { id } });
    if (!legue) {
      throw new NotFoundException('Legue not found');
    }
    await this.legueRepo.destroy({ where: { id } });
    return `This action removes a #${id} legue`;
  }
}
