import { Module } from '@nestjs/common';
import { LeguesService } from './legues.service';
import { LeguesController } from './legues.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Legue } from './models/legue.model';

@Module({
  imports: [SequelizeModule.forFeature([Legue])],
  controllers: [LeguesController],
  providers: [LeguesService],
})
export class LeguesModule {}
