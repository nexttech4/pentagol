import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsUrl } from 'class-validator';

export class CreateTeamDto {
  @ApiProperty({ example: 'Barcelona', description: 'Team name' })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({
    example: 'https://cdn-icons-png.flaticon.com/512/824/824748.png',
    description: 'Image url',
  })
  @IsUrl()
  logo: string;
}
