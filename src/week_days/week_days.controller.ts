import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import { WeekDaysService } from './week_days.service';
import { CreateWeekDayDto } from './dto/create-week_day.dto';
import { UpdateWeekDayDto } from './dto/update-week_day.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { WeekDay } from './model/week_day.model';

@ApiTags('WeekDays')
@Controller('week-days')
export class WeekDaysController {
  constructor(private readonly weekDaysService: WeekDaysService) {}

  @ApiOperation({ summary: 'Create a new Week day' })
  @ApiResponse({ status: 201, type: WeekDay })
  @HttpCode(HttpStatus.CREATED)
  @Post()
  create(@Body() createWeekDayDto: CreateWeekDayDto) {
    return this.weekDaysService.create(createWeekDayDto);
  }

  @ApiOperation({ summary: 'Find all Week days' })
  @ApiResponse({ status: 200, type: [WeekDay] })
  @HttpCode(HttpStatus.OK)
  @Get()
  findAll() {
    return this.weekDaysService.findAll();
  }

  @ApiOperation({ summary: 'Find one Week day by Id' })
  @ApiResponse({ status: 200, type: WeekDay })
  @HttpCode(HttpStatus.OK)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.weekDaysService.findOne(+id);
  }

  @ApiOperation({ summary: 'Update a Week day by Id' })
  @ApiResponse({ status: 200, type: WeekDay })
  @HttpCode(HttpStatus.OK)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateWeekDayDto: UpdateWeekDayDto) {
    return this.weekDaysService.update(+id, updateWeekDayDto);
  }

  @ApiOperation({ summary: 'Delete a Week day by Id' })
  @ApiResponse({ status: 200, type: WeekDay })
  @HttpCode(HttpStatus.OK)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.weekDaysService.remove(+id);
  }
}
