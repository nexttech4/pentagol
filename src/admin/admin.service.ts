import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateAdminDto } from './dto/create-admin.dto';
import { UpdateAdminDto } from './dto/update-admin.dto';
import { InjectModel } from '@nestjs/sequelize';
import { Admin } from './model/admin.model';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { Response } from 'express';
import { LoginAdminDto } from './dto/login-admin.dto';

@Injectable()
export class AdminService {
  constructor(
    @InjectModel(Admin) private readonly adminRepo: typeof Admin,
    private readonly jwtService: JwtService,
  ) {}
  async register(createAdminDto: CreateAdminDto) {
    const { login, password, confirm_password } = createAdminDto;
    const candidate = await this.adminRepo.findOne({ where: { login } });
    if (candidate) {
      throw new BadRequestException('This admin already exists');
    }
    if (password !== confirm_password) {
      throw new BadRequestException('Password is incorrect');
    }
    const hashed_password = await bcrypt.hash(password, 7);
    const admin = await this.adminRepo.create({
      ...createAdminDto,
      hashed_password,
      is_creator: true,
    });
    const response = {
      message: 'Admin registration successful',
      admin,
    };
    return response;
  }

  async login(loginAdminDto: LoginAdminDto, res: Response) {
    const { login, password } = loginAdminDto;
    const admin = await this.adminRepo.findOne({ where: { login } });
    if (!admin) {
      throw new BadRequestException('Login or password incorrect');
    }
    const IsMatchPass = await bcrypt.compare(password, admin.hashed_password);
    if (!IsMatchPass) {
      throw new BadRequestException('Login or password incorrect');
    }
    const updateAdmin = await this.adminRepo.update(
      { is_active: true },
      { where: { login }, returning: true },
    );
    const tokens = await this.getTokens(updateAdmin[1][0]);
    const hashed_refresh_token = await bcrypt.hash(tokens.refresh_token, 7);
    await this.adminRepo.update(
      { hashed_refresh_token },
      { where: { login }, returning: true },
    );
    res.cookie('refresh_token', tokens.refresh_token, {
      maxAge: 30 * 24 * 3600 * 1000,
      httpOnly: true,
    });
    const response = {
      message: 'Login is successful',
      tokens: tokens,
    };
  }

  async logout(refreshToken: string, res: Response) {
    const userData = await this.jwtService.verify(refreshToken, {
      secret: process.env.REFRESH_TOKEN_KEY,
    });
    if (!userData) {
      throw new ForbiddenException('User not found');
    }

    const updatedAdmin = await this.adminRepo.update(
      { hashed_refresh_token: null },
      { where: { id: userData.id }, returning: true },
    );
    res.clearCookie('refresh_token');
    const response = {
      message: 'User logged out successfully',
      admin: updatedAdmin[1][0],
    };
    return response;
  }
  findAll() {
    return this.adminRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const admin = await this.adminRepo.findOne({ where: { id } });
    if (!admin) {
      throw new NotFoundException('Admin not found');
    }
    return admin;
  }

  async update(id: number, updateAdminDto: UpdateAdminDto) {
    const admin = await this.adminRepo.findOne({ where: { id } });
    if (!admin) {
      throw new NotFoundException('Admin not found');
    }
    const updatedAdmin = await this.adminRepo.update(
      { login: updateAdminDto.login },
      { where: { id }, returning: true },
    );
    return updatedAdmin;
  }

  async remove(id: number) {
    const admin = await this.adminRepo.findOne({ where: { id } });
    if (!admin) {
      throw new NotFoundException('Admin not found');
    }
    await this.adminRepo.destroy({ where: { id } });
    return `This action removes a #${id} admin`;
  }

  async getTokens(admin: Admin) {
    const jwtPayload = {
      id: admin.id,
      is_active: admin.is_active,
      is_creator: admin.is_creator,
    };
    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.signAsync(jwtPayload, {
        secret: process.env.ACCESS_TOKEN_KEY,
        expiresIn: process.env.ACCESS_TOKEN_TIME,
      }),
      this.jwtService.signAsync(jwtPayload, {
        secret: process.env.REFRESH_TOKEN_KEY,
        expiresIn: process.env.REFRESH_TOKEN_TIME,
      }),
    ]);
    return {
      access_token: accessToken,
      refresh_token: refreshToken,
    };
  }
}
