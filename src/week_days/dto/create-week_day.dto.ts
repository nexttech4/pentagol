import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateWeekDayDto {
  @ApiProperty({ example: 'monday', description: 'Week day name' })
  @IsString()
  name: string;
}
