import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateMatchDto {
  @ApiProperty({ example: 1, description: 'Foreign key, legue' })
  @IsNumber()
  legue_id: number;

  @ApiProperty({ example: 1, description: 'Foreign key, team' })
  @IsNumber()
  host_id: number;

  @ApiProperty({ example: 1, description: 'Foreign key, team' })
  @IsNumber()
  gost_id: number;

  @ApiProperty({ example: '3:0', description: 'Score' })
  @IsNotEmpty()
  @IsString()
  score: string;

  @ApiProperty({ example: '2023-06-01', description: 'Date' })
  @IsNotEmpty()
  @IsDateString()
  date: Date;

  @ApiProperty({ example: '11:00', description: 'Time' })
  @IsNotEmpty()
  time: Date;

  @ApiProperty({ example: 1, description: 'Foreign key, week day' })
  @IsNotEmpty()
  week_days_id: number;
}
