import { ApiProperty } from '@nestjs/swagger';
import { Column, DataType, Model, Table } from 'sequelize-typescript';

@Table({ tableName: 'teams' })
export class Team extends Model<Team> {
  @ApiProperty({ example: 1, description: 'Primary key' })
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @ApiProperty({ example: 'Barcelona', description: 'Team name' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;

  @ApiProperty({
    example: 'https://cdn-icons-png.flaticon.com/512/824/824748.png',
    description: 'Logo URL',
  })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  logo: string;

  @ApiProperty({
    example: 10,
    description: 'Number of games',
  })
  @Column({
    type: DataType.SMALLINT,
    allowNull: false,
  })
  number_of_games: number;

  @ApiProperty({
    example: 10,
    description: 'Scored Points',
  })
  @Column({
    type: DataType.SMALLINT,
    allowNull: false,
  })
  scored_points: number;

  @ApiProperty({
    example: 10,
    description: 'Win',
  })
  @Column({
    type: DataType.SMALLINT,
    allowNull: false,
  })
  win: number;

  @ApiProperty({
    example: 10,
    description: 'Draw',
  })
  @Column({
    type: DataType.SMALLINT,
    allowNull: false,
  })
  draw: number;

  @ApiProperty({
    example: 10,
    description: 'Lose',
  })
  @Column({
    type: DataType.SMALLINT,
    allowNull: false,
  })
  lose: number;

  @ApiProperty({
    example: 10,
    description: 'Golas',
  })
  @Column({
    type: DataType.SMALLINT,
    allowNull: false,
  })
  golas: number;

  @ApiProperty({
    example: 10,
    description: 'Misses',
  })
  @Column({
    type: DataType.SMALLINT,
    allowNull: false,
  })
  misses: number;

  @ApiProperty({
    example: 10,
    description: 'Goals difference',
  })
  @Column({
    type: DataType.SMALLINT,
    allowNull: false,
  })
  goal_difference: number;
}
