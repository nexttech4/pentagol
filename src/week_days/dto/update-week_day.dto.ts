import { PartialType } from '@nestjs/mapped-types';
import { CreateWeekDayDto } from './create-week_day.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class UpdateWeekDayDto {
  @ApiProperty({ example: 'monday', description: 'Week day name' })
  @IsString()
  name: string;
}
