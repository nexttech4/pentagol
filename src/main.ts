import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
const start = async () => {
  try {
    const PORT = process.env.PORT || 5000;

    const app = await NestFactory.create(AppModule);
    app.setGlobalPrefix('api');
    app.useGlobalPipes(new ValidationPipe());

    const config = new DocumentBuilder()
      .setTitle('PentaGOL')
      .setDescription('This web application is for European football fans')
      .setVersion('1.0.0')
      .addTag('NodeJS, React, NestJS, PostgreSQL, Sequelize and Swagger')
      .setBasePath('api')
      .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api/pentagol-docs', app, document);
    app.listen(PORT, () => {
      console.log(`Server running on the ${PORT} port`);
    });
  } catch (error) {
    console.log(error);
  }
};
start();
