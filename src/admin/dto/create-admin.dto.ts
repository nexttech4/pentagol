import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsString,
  IsStrongPassword,
  MinLength,
} from 'class-validator';

export class CreateAdminDto {
  @ApiProperty({ example: '1abcdef', description: 'Admin login' })
  @IsNotEmpty()
  @IsString()
  login: string;

  @ApiProperty({
    example: '$Tr0ngPa$$w0rd',
    description: 'Admin password',
    minimum: 8,
  })
  @IsNotEmpty()
  @IsString()
  @IsStrongPassword()
  @MinLength(8)
  password: string;

  @ApiProperty({
    example: '$Tr0ngPa$$w0rd',
    description: 'Confirm                                password',
    minimum: 8,
  })
  @IsNotEmpty()
  @IsString()
  @IsStrongPassword()
  @MinLength(8)
  confirm_password: string;
}
