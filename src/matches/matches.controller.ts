import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import { MatchesService } from './matches.service';
import { CreateMatchDto } from './dto/create-match.dto';
import { UpdateMatchDto } from './dto/update-match.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Match } from './models/match.model';

@ApiTags('Matches')
@Controller('matches')
export class MatchesController {
  constructor(private readonly matchesService: MatchesService) {}

  @ApiOperation({ summary: 'Create Match' })
  @ApiResponse({ status: 201, type: Match })
  @HttpCode(HttpStatus.CREATED)
  @Post()
  create(@Body() createMatchDto: CreateMatchDto) {
    return this.matchesService.create(createMatchDto);
  }

  @ApiOperation({ summary: 'Find all Matches' })
  @ApiResponse({ status: 200, type: [Match] })
  @HttpCode(HttpStatus.OK)
  @Get()
  findAll() {
    return this.matchesService.findAll();
  }

  @ApiOperation({ summary: 'Find one Match by Id' })
  @ApiResponse({ status: 200, type: Match })
  @HttpCode(HttpStatus.OK)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.matchesService.findOne(+id);
  }

  @ApiOperation({ summary: 'Update a Match by Id' })
  @ApiResponse({ status: 200, type: Match })
  @HttpCode(HttpStatus.OK)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateMatchDto: UpdateMatchDto) {
    return this.matchesService.update(+id, updateMatchDto);
  }

  @ApiOperation({ summary: 'Delete a Match by Id' })
  @ApiResponse({ status: 200, type: Match })
  @HttpCode(HttpStatus.OK)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.matchesService.remove(+id);
  }
}
