import { Module } from '@nestjs/common';
import { MatchesService } from './matches.service';
import { MatchesController } from './matches.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Match } from './models/match.model';
import { Legue } from '../legues/models/legue.model';
import { WeekDay } from '../week_days/model/week_day.model';
import { Team } from '../teams/model/team.model';

@Module({
  imports: [SequelizeModule.forFeature([Match, Legue, WeekDay, Team])],
  controllers: [MatchesController],
  providers: [MatchesService],
})
export class MatchesModule {}
