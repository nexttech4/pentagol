import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMatchDto } from './dto/create-match.dto';
import { UpdateMatchDto } from './dto/update-match.dto';
import { InjectModel } from '@nestjs/sequelize';
import { Match } from './models/match.model';

@Injectable()
export class MatchesService {
  constructor(@InjectModel(Match) private readonly matchRepo: typeof Match) {}
  async create(createMatchDto: CreateMatchDto) {
    const match = await this.matchRepo.create(createMatchDto);
    return match;
  }

  async findAll() {
    const matches = await this.matchRepo.findAll({ include: { all: true } });
    return matches;
  }

  async findOne(id: number) {
    const match = await this.matchRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!match) {
      throw new NotFoundException('Match not found');
    }
    return match;
  }

  async update(id: number, updateMatchDto: UpdateMatchDto) {
    const match = await this.matchRepo.findOne({ where: { id } });
    if (!match) {
      throw new NotFoundException('Match not found');
    }
    const updateMatch = await this.matchRepo.update(
      { ...updateMatchDto },
      { where: { id }, returning: true },
    );
    return updateMatch[1][0];
  }

  async remove(id: number) {
    const match = await this.matchRepo.findOne({ where: { id } });
    if (!match) {
      throw new NotFoundException('Match not found');
    }
    await this.matchRepo.destroy({ where: { id } });
    return `This action removes a #${id} match`;
  }
}
