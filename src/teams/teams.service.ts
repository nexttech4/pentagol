import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateTeamDto } from './dto/create-team.dto';
import { UpdateTeamDto } from './dto/update-team.dto';
import { InjectModel } from '@nestjs/sequelize';
import { Team } from './model/team.model';

@Injectable()
export class TeamsService {
  constructor(@InjectModel(Team) private readonly teamRepo: typeof Team) {}
  async create(createTeamDto: CreateTeamDto) {
    const team = await this.teamRepo.findOne({
      where: { name: createTeamDto.name },
    });
    if (team) {
      throw new BadRequestException('This team already exists');
    }
    const newTeam = await this.teamRepo.create(createTeamDto);
    const response = {
      message: 'Team added successfully',
      team: newTeam,
    };
    return response;
  }

  findAll() {
    return this.teamRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const team = await this.teamRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!team) {
      throw new NotFoundException('Team not found');
    }
    return team;
  }

  async update(id: number, updateTeamDto: UpdateTeamDto) {
    const team = await this.teamRepo.findOne({ where: { id } });
    if (!team) {
      throw new NotFoundException('Team not found');
    }
    const teamName = await this.teamRepo.findOne({
      where: { name: updateTeamDto.name },
    });
    if (teamName.name == team.name) {
      throw new BadRequestException('Team already exists');
    }
    const updateTeam = await this.teamRepo.update(
      { ...updateTeamDto },
      { where: { id }, returning: true },
    );
    const response = {
      message: 'Updated team successfully',
      team: updateTeam[1][0],
    };
    return response;
  }

  async remove(id: number) {
    const team = await this.teamRepo.findOne({ where: { id } });
    if (!team) {
      throw new NotFoundException('Team not found');
    }
    await this.teamRepo.destroy({ where: { id } });
    return `This action removes a   ${team.name} team`;
  }
}
