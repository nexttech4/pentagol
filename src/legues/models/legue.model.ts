import { ApiProperty } from '@nestjs/swagger';
import { Column, DataType, Model, Table } from 'sequelize-typescript';

@Table({ tableName: 'legues' })
export class Legue extends Model<Legue> {
  @ApiProperty({ example: 1, description: 'Primary key' })
  @Column({
    type: DataType.SMALLINT,
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @ApiProperty({ example: 'LaLiga', description: 'Legue name' })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  name: string;
}
