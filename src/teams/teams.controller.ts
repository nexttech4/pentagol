import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import { TeamsService } from './teams.service';
import { CreateTeamDto } from './dto/create-team.dto';
import { UpdateTeamDto } from './dto/update-team.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Team } from './model/team.model';

@ApiTags('Teams-Jamoalar')
@Controller('teams')
export class TeamsController {
  constructor(private readonly teamsService: TeamsService) {}

  @ApiOperation({ summary: 'Adding a new team' })
  @ApiResponse({ status: 201, type: Team })
  @HttpCode(HttpStatus.CREATED)
  @Post()
  create(@Body() createTeamDto: CreateTeamDto) {
    return this.teamsService.create(createTeamDto);
  }

  @ApiOperation({ summary: 'Find all teams' })
  @ApiResponse({ status: 200, type: [Team] })
  @HttpCode(HttpStatus.OK)
  @Get()
  findAll() {
    return this.teamsService.findAll();
  }

  @ApiOperation({ summary: 'Find a team by ID' })
  @ApiResponse({ status: 200, type: Team })
  @HttpCode(HttpStatus.OK)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.teamsService.findOne(+id);
  }

  @ApiOperation({ summary: 'Update a team by ID' })
  @ApiResponse({ status: 200, type: Team })
  @HttpCode(HttpStatus.OK)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTeamDto: UpdateTeamDto) {
    return this.teamsService.update(+id, updateTeamDto);
  }

  @ApiOperation({ summary: 'Find a team by ID' })
  @ApiResponse({ status: 200, type: Team })
  @HttpCode(HttpStatus.OK)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.teamsService.remove(+id);
  }
}
