import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import { LeguesService } from './legues.service';
import { CreateLegueDto } from './dto/create-legue.dto';
import { UpdateLegueDto } from './dto/update-legue.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Legue } from './models/legue.model';

@ApiTags('Legues')
@Controller('legues')
export class LeguesController {
  constructor(private readonly leguesService: LeguesService) {}

  @ApiOperation({ summary: 'Create a new legue' })
  @ApiResponse({ status: 201, type: Legue })
  @HttpCode(HttpStatus.CREATED)
  @Post()
  create(@Body() createLegueDto: CreateLegueDto) {
    return this.leguesService.create(createLegueDto);
  }

  @ApiOperation({ summary: 'Find all legues' })
  @ApiResponse({ status: 200, type: [Legue] })
  @HttpCode(HttpStatus.OK)
  @Get()
  findAll() {
    return this.leguesService.findAll();
  }

  @ApiOperation({ summary: 'Find one legue by Id' })
  @ApiResponse({ status: 200, type: Legue })
  @HttpCode(HttpStatus.OK)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.leguesService.findOne(+id);
  }

  @ApiOperation({ summary: 'Update a legue by Id' })
  @ApiResponse({ status: 200, type: Legue })
  @HttpCode(HttpStatus.OK)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateLegueDto: UpdateLegueDto) {
    return this.leguesService.update(+id, updateLegueDto);
  }

  @ApiOperation({ summary: 'Delete a legue by Id' })
  @ApiResponse({ status: 200, type: Legue })
  @HttpCode(HttpStatus.OK)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.leguesService.remove(+id);
  }
}
