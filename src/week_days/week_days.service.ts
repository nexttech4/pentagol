import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateWeekDayDto } from './dto/create-week_day.dto';
import { UpdateWeekDayDto } from './dto/update-week_day.dto';
import { InjectModel } from '@nestjs/sequelize';
import { WeekDay } from './model/week_day.model';

@Injectable()
export class WeekDaysService {
  constructor(
    @InjectModel(WeekDay) private readonly weekDayRepo: typeof WeekDay,
  ) {}
  async create(createWeekDayDto: CreateWeekDayDto) {
    const week_day = await this.weekDayRepo.findOne({
      where: { name: createWeekDayDto.name },
    });
    if (week_day) {
      throw new BadRequestException('This WeekDay is already exist');
    }
    const weekDay = await this.weekDayRepo.create(createWeekDayDto);
    return weekDay;
  }

  findAll() {
    return this.weekDayRepo.findAll({ include: { all: true } });
  }

  async findOne(id: number) {
    const week_day = await this.weekDayRepo.findOne({
      where: { id },
      include: { all: true },
    });
    if (!week_day) {
      throw new NotFoundException('week day not found');
    }
    return week_day;
  }

  async update(id: number, updateWeekDayDto: UpdateWeekDayDto) {
    const week_day = await this.weekDayRepo.findOne({ where: { id } });
    if (!week_day) {
      throw new NotFoundException('week day not found');
    }
    const updateWeekDay = await this.weekDayRepo.update(
      { ...updateWeekDayDto },
      { where: { id }, returning: true },
    );
    return updateWeekDay[1][0];
  }

  async remove(id: number) {
    const week_day = await this.weekDayRepo.findOne({ where: { id } });
    if (!week_day) {
      throw new NotFoundException('week day not found');
    }
    await this.weekDayRepo.destroy({ where: { id } });
    return `This action removes a #${id} weekDay`;
  }
}
